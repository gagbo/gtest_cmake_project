add_executable(test_lib test_lib.cpp)
target_link_libraries(test_lib PUBLIC baselib)
target_link_libraries(test_lib PRIVATE gtest_main)

include(CTest)
include(GoogleTest)
gtest_discover_tests(test_lib)
