#include "gtest/gtest.h"
#include "project/lib.hpp"

TEST(BasicTest, DefaultConstructor) {
  Basic instance;
  EXPECT_EQ(instance.get_name(), "Test");
}
