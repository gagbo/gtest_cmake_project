# Basic GoogleTest C++ project

This project aims to be a good starting point when developing a new app in C++.
The structure is inspired by this
[good tutorial](https://cliutils.gitlab.io/modern-cmake/chapters/basics/structure.html)

If you want to see a CMake 3.11+ version using the new FetchContent module
instead of a git submodule, you can check my
[sister project](https://github.com/gagbo/catch2_cmake_project) where you can
simply replace the Catch2 dependency in FetchContent with a gtest one
